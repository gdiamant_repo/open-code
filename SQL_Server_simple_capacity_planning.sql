-- ==========================================================================================
-- Author:		GDI
-- Creation date: 04 07 2019
-- Description:	capacity planning / all databases / last 365 days / sp_spaceused method
-- =========================================================================================
SET ANSI_NULLS ON
GO

SET NOCOUNT ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (
		SELECT *
		FROM sys.objects
		WHERE object_id = OBJECT_ID(N'database_gain_history')
			AND type IN (
				N'P'
				,N'PC'
				)
		)
	DROP PROCEDURE database_gain_history
GO

CREATE PROCEDURE database_gain_history @localdbname NVARCHAR(255)
	,@days INTEGER
AS
BEGIN
	DECLARE @Temp_Table TABLE (
		DB NVARCHAR(128)
		,Table_name NVARCHAR(128)
		,Schema_name NVARCHAR(128)
		,Date_creation DATETIME
		,rows BIGINT
		,reserved NVARCHAR(64)
		,data_size NVARCHAR(64)
		,index_size NVARCHAR(64)
		,unusued NVARCHAR(64)
		)
	DECLARE @Final_Table TABLE (
		dbname NVARCHAR(64)
		,Id_num INTEGER IDENTITY(1, 1)
		,DateAAAAMM NVARCHAR(64)
		,SizeGain_Mb BIGINT
		,SumSizeGain_Mb BIGINT
		,TotalDatabaseSize_Mb BIGINT
		)
	DECLARE @DatabaseActualSize_Mb TABLE (ActualSize NVARCHAR(64))
	DECLARE @NRows INTEGER
	DECLARE @SQL VARCHAR(max)
	DECLARE @MaxId_num INTEGER
	DECLARE @TempValue1 BIGINT
	DECLARE @TempValue2 BIGINT

	----- setting  value, do not change 
	SET @SQL = ''
	SET @NRows = NULL

	-----------------------------------
	SELECT @SQL = @SQL + 'SELECT ''' + @localdbname + ''', TABLE_NAME COLLATE Latin1_General_BIN, TABLE_SCHEMA COLLATE Latin1_General_BIN, ST.create_date FROM [' + @localdbname + '].INFORMATION_SCHEMA.TABLES SCT JOIN [' + @localdbname + '].sys.tables ST ON ST.name= SCT.TABLE_NAME WHERE ST.create_date >= (GETDATE() - ' + convert(NVARCHAR, @days) + ') AND TABLE_TYPE = ''BASE TABLE'' ORDER BY 4 ASC UNION ALL '

	SET @SQL = SUBSTRING(@SQL, 1, LEN(@SQL) - 10) -- get out the lat ' union all' 

	PRINT @SQL

	INSERT INTO @Temp_Table (
		DB
		,Table_name
		,Schema_name
		,Date_creation
		)
	EXEC (@SQL)

	SET @NRows = @@ROWCOUNT

	IF (@NRows = 0)
		PRINT 'No data for ' + @localdbname

	DECLARE @DB NVARCHAR(128)
		,@Table_name NVARCHAR(128)
		,@Schema_name NVARCHAR(128)
		,@Date_creation NVARCHAR(64)
	DECLARE @Spaceused_table TABLE (
		NAME SYSNAME
		,rows BIGINT
		,reserved VARCHAR(64)
		,data_size VARCHAR(64)
		,index_size VARCHAR(64)
		,unusued VARCHAR(64)
		)

	DECLARE C CURSOR LOCAL FORWARD_ONLY STATIC READ_ONLY
	FOR
	SELECT DB
		,Table_name
		,Schema_name
		,Date_creation
	FROM @Temp_Table

	OPEN C

	FETCH C
	INTO @DB
		,@Table_name
		,@Schema_name
		,@Date_creation

	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @SQL = 'EXEC ' + @DB + '..sp_spaceused ''' + quotename(@Schema_name) + '.' + quotename(@Table_name) + ''''

		BEGIN TRY
			INSERT INTO @Spaceused_table
			EXEC (@SQL)
		END TRY

		BEGIN CATCH
			SELECT ERROR_NUMBER() AS ErrorNumber
				,ERROR_MESSAGE() AS ErrorMessage;
		END CATCH

		UPDATE @Spaceused_table
		SET reserved = REPLACE(reserved, ' KB', '')
			,data_size = REPLACE(data_size, ' KB', '')
			,index_size = REPLACE(index_size, ' KB', '')
			,unusued = REPLACE(unusued, ' KB', '')

		UPDATE @Temp_Table
		SET rows = spaceused.rows
			,reserved = spaceused.reserved
			,data_size = spaceused.data_size
			,index_size = spaceused.index_size
			,unusued = spaceused.unusued
		FROM @Spaceused_table AS spaceused
		WHERE Table_name = @Table_name

		DELETE
		FROM @Spaceused_table

		FETCH C
		INTO @DB
			,@Table_name
			,@Schema_name
			,@Date_creation
	END

	CLOSE C

	DEALLOCATE C

	DECLARE @total_rows FLOAT
	DECLARE @total_data_size FLOAT
	DECLARE @total_index_size FLOAT

	INSERT INTO @Final_Table (
		DateAAAAMM
		,SizeGain_Mb
		)
	SELECT convert(VARCHAR(7), Date_creation, 126) AS DATE
		,sum(round(((convert(BIGINT, data_size) + convert(BIGINT, index_size)) / 1024), 5)) AS SizeGain_Mb
	FROM @Temp_Table
	WHERE Date_creation >= (GETDATE() - @days)
	GROUP BY convert(VARCHAR(7), Date_creation, 126)
	ORDER BY convert(VARCHAR(7), Date_creation, 126) ASC

	UPDATE @Final_Table
	SET dbname = @localdbname

	DECLARE @Id_num INTEGER
		,@DateAAAAMM VARCHAR(64)
		,@SizeGain_Mb BIGINT
		,@SumSizeGain_Mb BIGINT
		,@TotalDatabaseSize_Mb NVARCHAR(64)

	SET @SumSizeGain_Mb = 0

	DECLARE C CURSOR LOCAL FORWARD_ONLY STATIC READ_ONLY
	FOR
	SELECT Id_num
		,DateAAAAMM
		,SizeGain_Mb
	FROM @Final_Table

	OPEN C

	FETCH C
	INTO @Id_num
		,@DateAAAAMM
		,@SizeGain_Mb

	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @SumSizeGain_Mb = convert(BIGINT, @SumSizeGain_Mb) + convert(BIGINT, @SizeGain_Mb)

		UPDATE @Final_Table
		SET SumSizeGain_Mb = @SumSizeGain_Mb
		WHERE Id_num = @Id_num

		FETCH C
		INTO @Id_num
			,@DateAAAAMM
			,@SizeGain_Mb
	END

	SET @MaxId_num = @Id_num

	CLOSE C

	DEALLOCATE C;

	-- Make the retro calculation of the global DB size using the last size  
	IF (@NRows != 0)
	BEGIN
		-- get last Data size 
		SELECT @SQL = 'USE ' + @localdbname + '; WITH dbSpacemaster
	AS (
		SELECT ''' + @localdbname + ''' AS DbName
			,fg.name AS FgName
			,count(f.name) AS NbFiles
			,sum(f.size / 128) AS CurrentSizeMB
			,sum(f.size / 128 - CAST(FILEPROPERTY(f.name, ''SpaceUsed'') AS INT) / 128) AS FreeSpaceMB
	    FROM sys.database_files f
		INNER JOIN sys.filegroups fg ON fg.data_space_id = f.data_space_id
		GROUP BY fg.name
		)
		SELECT CurrentSizeMB - FreeSpaceMB FROM dbSpacemaster'

		INSERT INTO @DatabaseActualSize_Mb
		EXEC (@SQL)

		UPDATE @Final_Table
		SET TotalDatabaseSize_Mb = (
				SELECT TOP 1 *
				FROM @DatabaseActualSize_Mb
				)

		WHILE @MaxId_num >= 0
		BEGIN
			SET @TempValue1 = (
					SELECT TotalDatabaseSize_Mb
					FROM @Final_Table
					WHERE Id_num = @MaxId_num
					)
			SET @TempValue2 = (
					SELECT SizeGain_Mb
					FROM @Final_Table
					WHERE Id_num = @MaxId_num - 1
					)

			UPDATE @Final_Table
			SET TotalDatabaseSize_Mb = (@TempValue1 - @TempValue2)
			WHERE Id_num = @MaxId_num - 1

			SET @MaxId_num = @MaxId_num - 1;
		END;

		SELECT dbname
			,Id_num
			,DateAAAAMM
			,SizeGain_Mb
			,TotalDatabaseSize_Mb
		FROM @Final_Table

		INSERT INTO #Summary_Table
		SELECT *
		FROM @Final_Table
	END
END
GO

DECLARE @databasename AS NVARCHAR(128)
DECLARE @return_value INT

CREATE TABLE #Summary_Table (
	dbname NVARCHAR(64)
	,Id_num INTEGER
	,DateAAAAMM NVARCHAR(64)
	,SizeGain_Mb BIGINT
	,SumSizeGain_Mb BIGINT
	,TotalDatabaseSize_Mb BIGINT
	)

DECLARE db_list CURSOR
FOR
SELECT [name] AS databasename
FROM master.dbo.sysdatabases
WHERE [name] NOT LIKE 'ADM_%'
	AND dbid > 4

OPEN db_list

FETCH NEXT
FROM db_list
INTO @databasename

WHILE @@FETCH_STATUS = 0
BEGIN
	DECLARE @sqlCmd AS NVARCHAR(4000)

	SET @sqlCmd = N'EXEC [dbo].[database_gain_history] @localdbname = ' + @databasename + ',@days = 365'

	EXEC sp_executesql @sqlCmd

	FETCH NEXT
	FROM db_list
	INTO @databasename
END

CLOSE db_list

DEALLOCATE db_list

DECLARE @Summary_DateAAAAMM NVARCHAR(64)
DECLARE @Summary_SizeGain_Mb BIGINT
DECLARE @Summary_TotalDatabaseSize_Mb BIGINT

DECLARE summary_list CURSOR
FOR
SELECT DateAAAAMM
	,SizeGain_Mb
	,TotalDatabaseSize_Mb
FROM #Summary_Table

OPEN summary_list

FETCH NEXT
FROM summary_list
INTO @Summary_DateAAAAMM
	,@Summary_SizeGain_Mb
	,@Summary_TotalDatabaseSize_Mb

WHILE @@FETCH_STATUS = 0
BEGIN
	FETCH NEXT
	FROM summary_list
	INTO @Summary_DateAAAAMM
		,@Summary_SizeGain_Mb
		,@Summary_TotalDatabaseSize_Mb
END

CLOSE summary_list

DEALLOCATE summary_list

SELECT @@servername AS InstanceName
	,DateAAAAMM
	,SUM(SizeGain_Mb) AS SizeGain_Mb
	,SUM(TotalDatabaseSize_Mb) AS TotalDatabaseSize_Mb
FROM #Summary_Table
GROUP BY DateAAAAMM

DROP PROCEDURE database_gain_history
GO

DROP TABLE #Summary_Table
GO

